<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cliente extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(false);
        $this->load->model('cliente_model');
        $this->load->model('admin_model');
        $this->admin_model->logado();
    }

    public function cadastrar()
    {
        $dados['menu_ativo'] = null;
        $dados['erros'] = null;
        $dados['sucesso'] = null;
        $dados['clientes'] = null;
        
        $this->form_validation->set_rules('cnpj', 'Cnpj', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required|min_length[3]');
        
        if ($this->form_validation->run() == FALSE) {
            $dados['erros'] = validation_errors('<li>', '</li>');
            $this->load->view('admin/cadastrar_cliente_view', $dados);
        } else {            
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'jpg';
            $config['encrypt_name'] = TRUE;
            $config['max_size'] = 1024 * 2;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if ($this->upload->do_upload('arquivo')) {
                
                $dados['clientes'] = array(
                    'nome' => $this->input->post('nome'),
                    'cnpj' => $this->input->post('cnpj'),
                    'logo' => '/uploads/' . $this->upload->data('file_name')
                );
                
                $this->cliente_model->salvar($dados['clientes']);
                $this->session->set_flashdata('msg', '<div class="alert alert-success">Cliente cadastrado com Sucesso!</div>');
                redirect('admin/cliente', 'refresh');
            } else {
                $dados['erros'] = $this->upload->display_errors('<li>', '</li>');
                $this->load->view('admin/cadastrar_cliente_view', $dados);
            }
        }
    }

    public function index()
    {
        $dados['menu_ativo'] = 'clientes';
        $dados['clientes'] = null;
        $dados['clientes'] = $this->cliente_model->listar();
        $this->load->view('admin/lista_clientes_view', $dados);
    }

    public function editar()
    {
        $dados['menu_ativo'] = null;
        $dados['erros'] = null;
        $dados['sucesso'] = null;
        $dados['clientes'] = $this->cliente_model->buscar($this->input->get('cliente_id'));
        $this->load->view('admin/editar_cliente_view', $dados);
    }

    public function atualizar()
    {
        $dados['menu_ativo'] = null;
        $dados['clientes'] = null;
        $dados['erros'] = null;
        $dados['sucesso'] = null;
        
        $this->form_validation->set_rules('cliente_id', 'cliente_id', 'trim|required|integer');
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('cnpj', 'cnpj', 'trim|required|min_length[3]');
        
        if ($this->form_validation->run() == FALSE) {
            $dados['erros'] = validation_errors('<li>', '</li>');
            $this->load->view('admin/editar_cliente_view', $dados);
        } else {
            $dados['clientes'] = array(
                'nome' => $this->input->post('nome'),
                'cnpj' => $this->input->post('cnpj'),
                'cliente_id' => $this->input->post('cliente_id')
            );
            $this->cliente_model->atualizar($dados['clientes']);
            $dados['clientes'] = $this->cliente_model->listar();
            $this->load->view('admin/lista_clientes_view', $dados);
        }
    }

    public function deletar()
    {
        $dados['menu_ativo'] = 'clientes';
        $dados['clientes'] = null;
        $dados['inscritos'] = $this->cliente_model->deletar($this->input->get('cliente_id'));
        $dados['clientes'] = $this->cliente_model->listar();
        $this->load->view('admin/lista_clientes_view', $dados);
    }
}