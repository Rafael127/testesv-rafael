<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    public function troco()
    {
        $this->load->model('admin_model');
        $this->admin_model->logado();
        
        $this->load->model('troco_model');
        
        pd($this->troco_model->getQtdeNotas(289.99));
    }

    public function index()
    {
        $this->load->model('admin_model');
        $this->admin_model->logado();
        $dados['menu_ativo'] = 'inicio';
        $this->load->view('admin/admin_view', $dados);
    }

    public function login()
    {
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
        $this->form_validation->set_rules('senha', 'Senha', 'trim|required');
        
        $dados['erros'] = null;
        
        if ($this->form_validation->run() == FALSE) {
            $dados['erros'] = validation_errors();
            $this->load->view("admin/login_view", $dados);
        } else {
            $nome = $this->security->xss_clean($this->input->post("nome"));
            $senha = $this->security->xss_clean($this->input->post("senha"));
            if ('admin' == $nome and 'admin@sv' == $senha) {
                $dados_sessao = array(
                    'logado' => TRUE,
                    'nome_usuario' => $nome
                );
                $this->session->set_userdata($dados_sessao);
                redirect(base_url('admin'));
            } else {
                $dados['erros'] = "Usuario e/ou Senha invalidos!";
                $this->load->view("admin/login_view", $dados);
            }
        }
    }

    public function logout()
    {
        $data = array(
            'logado' => '',
            'nome_usuario' => ''
        );
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
        redirect(base_url('admin/home/login'), 'refresh');
    }
}
