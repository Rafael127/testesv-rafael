<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css" />
<script type="text/javascript"
	src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
<script type="text/javascript" charset="utf-8">
            $(document).ready(function () {
                $('#example').DataTable({
                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
                    }
                });
            });
            $(document).ready(function () {
                $('#example').DataTable();
            });
        </script>
<link rel="stylesheet"
	href="<?php echo base_url('includes/admin/css/bootstrap.css') ?>">
<link rel="stylesheet"
	href="<?php echo base_url('includes/admin/css/custom.css') ?>" />
<link rel="stylesheet"
	href="<?php echo base_url('includes/admin/css/font-awesome.css') ?>" />
</head>
<body>
	<div id="wrapper">
		<?php $this->load->view('admin/commons/menu'); ?>
		<div id="page-wrapper">
			<div id="page-inner">
				<div class="row" style="margin: 1px;">
					<br> <a class="btn btn-primary"
						href="<?php echo base_url('admin/cliente/cadastrar'); ?>"> Novo
						Cliente</a> <br> <br> <strong>
			  <?php
    echo $this->session->flashdata('msg');
    ?>
    </strong>
					<table id="example" class="display" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Inscrição</th>
								<th>Nome</th>
								<th>Cnpj</th>
								<th>Acões</th>
							</tr>
						</thead>

                    <?php if ($clientes == null) { ?>	
                        <tbody>
							<tr>
								<td>nenhum registro encontrado!</td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
                    <?php } else { ?>
                        <tbody>
                            <?php foreach ($clientes as $linha) { ?>	
                                <tr>
								<td><?= $linha['id'] ?></td>
								<td><?= $linha['nome'] ?></td>
								<td><?= $linha['cnpj'] ?></td>
								<td>
									<div class="row">
										<div class="col-md-12">
											<a class="btn btn-warning btn-xs"
												href="<?php echo base_url("admin/cliente/editar?cliente_id=".$linha['id']); ?>">
												Editar</a> <a class="btn btn-danger  btn-xs"
												href="<?php echo base_url("admin/cliente/deletar?cliente_id=".$linha['id']); ?>">
												Deletar</a>
										</div>
									</div>
								</td>
							</tr>
                            <?php } ?>	
                        </tbody>
                    <?php } ?>	
                </table>
				</div>
			</div>
		</div>
		<script type="text/javascript">
                // For demo to fit into DataTables site builder...
                $('#example')
                        .removeClass('display')
                        .addClass('table table-striped table-bordered');
            </script>
	</div>
	<?php $this->load->view('admin/commons/rodape'); ?>
</body>
</html>