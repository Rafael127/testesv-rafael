<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css' />
<link rel="stylesheet"
	href="<?php echo base_url('includes/admin/css/bootstrap.css') ?>">
<link rel="stylesheet"
	href="<?php echo base_url('includes/admin/css/custom.css') ?>" />
<link rel="stylesheet"
	href="<?php echo base_url('includes/admin/css/font-awesome.css') ?>" />
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.10/angular.min.js"></script>
<script src="<?php echo base_url('includes/js/ngMask.min.js') ?>"></script>

<script>
    var validationApp = angular.module('validationApp', ['ngMask']);
   
    validationApp.directive('cnpjValido', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {	
    
                function validacnpj(cnpj) {                                   	 
                	  cnpj = cnpj.replace(/[^\d]+/g,'');
                	  if (cnpj == '') return false;
                	  if (cnpj.length != 14)
                	      return false;
                	  // LINHA 10 - Elimina CNPJs invalidos conhecidos
                	  if (cnpj == "00000000000000" || 
                	      cnpj == "11111111111111" || 
                	      cnpj == "22222222222222" || 
                	      cnpj == "33333333333333" || 
                	      cnpj == "44444444444444" || 
                	      cnpj == "55555555555555" || 
                	      cnpj == "66666666666666" || 
                	      cnpj == "77777777777777" || 
                	      cnpj == "88888888888888" || 
                	      cnpj == "99999999999999")
                	      return false; // LINHA 21

                	  // Valida DVs LINHA 23 -
                	  tamanho = cnpj.length - 2
                	  numeros = cnpj.substring(0,tamanho);
                	  digitos = cnpj.substring(tamanho);
                	  soma = 0;
                	  pos = tamanho - 7;
                	  for (i = tamanho; i >= 1; i--) {
                	      soma += numeros.charAt(tamanho - i) * pos--;
                	      if (pos < 2)
                	          pos = 9;
                	  }
                	  resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                	  if (resultado != digitos.charAt(0))
                	      return false;

                	  tamanho = tamanho + 1;
                	  numeros = cnpj.substring(0,tamanho);
                	  soma = 0;
                	  pos = tamanho - 7;
                	  for (i = tamanho; i >= 1; i--) {
                	      soma += numeros.charAt(tamanho - i) * pos--;
                	      if (pos < 2)
                	          pos = 9;
                	  }
                	  resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                	  if (resultado != digitos.charAt(1))
                	        return false; // LINHA 49
                	  return true; 
              	}
                 
                scope.$watch(attrs.ngModel, function () {           		                	
                    if (elem[0].value.length == 18) {                            	                                
                 		ctrl.$setValidity('cnpjValido', validacnpj(elem[0].value));             
                    } else{
                    	ctrl.$setValidity('cnpjValido', true);
                    }      
                });
            }
        };
    });

    //criar angular controller
    validationApp.controller('mainController', function($scope) {
     // função para enviar o formulário depois que a validação estiver ok           
     $scope.submitForm = function(isValid) {
    
         // verifica se o formulário é válido
         if (isValid) {
             alert('Formulário OK');
         }
    
     };
    
    });
    
</script>
</head>
<body ng-app="validationApp" ng-controller="mainController"
	ng-init="cnpj='<?php echo (isset($clientes['cnpj']) and ! empty($clientes['cnpj'])) ? $clientes['cnpj'] : set_value('cnpj')?>';nome='<?php echo (isset($clientes['nome']) and ! empty($clientes['nome'])) ? $clientes['nome'] : set_value('nome')?>'">
	<div id="wrapper">
		<?php $this->load->view('admin/commons/menu'); ?>
		<div id="page-wrapper">
			<div id="page-inner">
				<div class="row" style="margin: 1px;">
					<br>
					<form action="<?php echo base_url('admin/cliente/atualizar'); ?>"
						method="post" name="frm" novalidate>
						<div class="form-group">
							<div class="form-group col-md-12">
		<?php if ($erros): ?>
				<br>
								<div class="alert alert-danger">
									<ul>
    					<?= $erros; ?>
    				</ul>
								</div>
    		<?php endif; ?>
    
    		<?php if ($sucesso): ?>
    			<div class="alert alert-success"></div>
    		<?php endif; ?>
    	</div>

							<div class="row">

								<div class="form-group col-md-4">


									<input type="hidden" name="cliente_id" id="cliente_id"
										value=<?php echo (isset($clientes['id']) and ! empty($clientes['id'])) ? $clientes['id'] : set_value('cliente_id')?>>

									<label for="nome">Nome:</label> <input class="form-control"
										type="text" id="nome" name="nome"
										value=<?php echo (isset($clientes['nome']) and ! empty($clientes['nome'])) ? $clientes['nome'] : set_value('nome')?>>

									<label for="nome">Cnpj:</label> <input class="form-control"
										required type="text" id="cnpj" name="cnpj"
										mask='99.999.999/9999-99' ng-model="cnpj"
										value=<?php echo (isset($clientes['cnpj']) and ! empty($clientes['cnpj'])) ? $clientes['cnpj'] : set_value('cnpj')?>>
									<br>
									<button type="submit" ng-disabled="frm.$invalid"
										class="btn btn-primary">Salvar</button>


								</div>
							</div>
						</div>


					</form>
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('admin/commons/rodape'); ?>
</body>
</html>