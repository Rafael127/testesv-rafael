<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<link rel="stylesheet"
	href="<?php echo base_url('includes/bootstrap-3.3.7/css/bootstrap.min.css') ?>">
</head>

<body style='background-color: #214761;'>
	<div class="container">
		<br> <br> <br> <br> <br> <br>
		<div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<div class="panel panel-primary">
					<div class="panel-body">			
                            <?php if ($erros): ?>
                                <div class="alert alert-danger">							
                                    <?= $erros; ?>							
                                </div>
                            <?php endif; ?>

                            <form method="post"
							action="<?php echo base_url('admin/home/login') ?>">
							<h3>Login</h3>

							<div class="form-group">
								<!--	<label for="nome">Login:</label> -->
								<input type="text" id="nome" name="nome"
									value="<?= set_value('nome') ?>" class="form-control"
									placeholder="Usuario" autofocus> <span class="text-danger"><?php echo form_error('nome'); ?></span>
							</div>

							<div class="form-group">
								<!--	<label for="senha">Senha:</label>   -->
								<input type="password" id="senha"
									value="<?= set_value('senha') ?>" name="senha"
									class="form-control" placeholder="Senha"> <span
									class="text-danger"><?php echo form_error('senha'); ?></span>
							</div>
							<input class="btn btn-primary btn-block" type="submit" value="Entrar">

						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4"></div>
	</div>

</body>
</html>

