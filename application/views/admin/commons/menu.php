<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="adjust-nav">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".sidebar-collapse">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"> <img height="80" width="80"
				src="<?php echo base_url('includes/imagens/loguespi.jpg')?>" />
			</a>

		</div>

		<span class="logout-spn"> <a
			href="<?php echo base_url('admin/home/logout'); ?>"
			style="color: #fff;">Sair do Sistema</a>
		</span>
	</div>
</div>
<!-- /. NAV TOP  -->
<nav class="navbar-default navbar-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav" id="main-menu">
			<li
				<?php echo ($menu_ativo == 'inicio') ? "class='active-link'" : ''; ?>><a
				href="<?php echo ($menu_ativo == 'inicio') ? '#' : base_url('admin'); ?>"><i
					class="fa fa-desktop "></i>Início</a></li>

			<li
				<?php echo ($menu_ativo == 'inscritos') ? "class='active-link'" : ''; ?>><a
				href="<?php echo ($menu_ativo == 'clientes') ? '#' : base_url('admin/cliente'); ?>"><i
					class="fa fa-users "></i>Listar Clientes</a></li>

		</ul>
	</div>

</nav>
<!-- /. NAV SIDE  -->