<?php

class Admin_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function logado()
    {
        $logado = $this->session->userdata('logado');
        if (! isset($logado) || $logado != true) {
            redirect('admin/home/login', 'refresh');
        }
    }
}
