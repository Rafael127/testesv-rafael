<?php

class Cliente_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function salvar($dados)
    {
        $this->db->trans_strict(FALSE);
        
        $this->db->trans_begin();
        $sql = "INSERT INTO cliente (nome, cnpj, logo ,dt_criacao) ";
        $sql = $sql . " VALUES(?, ?, ?, now())";
        
        $this->db->query($sql, array(
            $dados['nome'],
            $dados['cnpj'],
            $dados['logo']
        ));
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
    }

    public function buscar($cliente_id)
    {
        $query = $this->db->query("SELECT * FROM cliente where id = ? limit 1", array(
            $cliente_id
        ));
        return $query->row_array();
    }

    public function deletar($cliente_id)
    {
        $query = $this->db->query("delete from cliente where id = ? limit 1", array(
            $cliente_id
        ));
        return $this->db->affected_rows();
    }

    public function listar()
    {
        $sql = "SELECT * FROM cliente";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function atualizar($dados)
    {
        $query = $this->db->query("UPDATE cliente SET nome = ?, cnpj = ? WHERE id = ? limit 1", array(
            $dados['nome'],
            $dados['cnpj'],
            $dados['cliente_id']
        ));
        return $this->db->affected_rows();
    }
}
