<?php

class Troco_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getQtdeNotas($valor)
    {
        // $valor = 89.99;
        $troco = array();
        // var_dump($troco);
        
        $notas = array(
            1 => '100',
            2 => '50',
            3 => '20',
            4 => '10',
            5 => '5',
            6 => '2',
            7 => '1',
            8 => '0.5',
            9 => '0.25',
            10 => '0.1',
            11 => '0.05',
            12 => '0.01'
        );
        
        $i = 1;
        while ($notas[$i] > $valor) {
            $i = $i + 1;
        }
        $resto = - 1.0;
        // echo 'menor nota = '.$notas[$i];
        while ($resto != .00) {
            $quociente = floor($valor / $notas[$i]);
            $resto = $valor - ($quociente * $notas[$i]);
            $resto = round($resto, 2);
            if($quociente != 0)
                $troco[$notas[$i]] = $quociente;
            $valor = $resto;
            $i = $i + 1;
        }
        
        return ($troco);
    }
}
